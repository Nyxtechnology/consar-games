// A javascript-enhanced crossword puzzle [c] Jesse Weisbeck, MIT/GPL
(function($) {
	$(function() {
		// provide crossword entries in an array of objects like the following example
		// Position refers to the numerical order of an entry. Each position can have
		// two entries: an across entry and a down entry
		var puzzleData = [
				{
				clue: "Tu estado de cuenta no tiene CURP, ¿A que instancia debes dirigirte?",
				answer: "renapo",
				position: 1,
				orientation: "across",
				startx: 5,
				starty: 2
				},
				{
				clue: "El NSS registrado en tu cuenta puede ser incorrecto, ¿A que instancia debes dirigirte?",
				answer: "imss",
				position: 3,
				orientation: "across",
				startx: 6,
				starty: 10
				},
				{
				clue: "La CURP de tu estado de cuenta no está certificada, ¿A que instancia debes dirigirte?",
				answer: "renapo",
				position: 5,
				orientation: "across",
				startx: 1,
				starty: 8
				},
				{
				clue: "La CURP de tu estado de cuenta es incorrecta, ¿A que instancia debes dirigirte?",
				answer: "renapo",
				position: 7,
				orientation: "across",
				startx: 8,
				starty: 5
				},
				{
				clue: "El NSS registrado en el estado de cuenta es incorrecto, ¿A que instancia debes dirigirte?",
				answer: "imss",
				position: 8,
				orientation: "across",
				startx: 3,
				starty: 5
				},
				{
					clue: "El nombre registrado en tu estado de cuenta es incorrecto, ¿A que instancia debes dirigirte?",
					answer: "registrocivil",
					position: 1,
					orientation: "down",
					startx: 6,
					starty: 1
				},
				{
				clue: "Tu estado de cuenta no tiene RFC o se encuentra sin homoclave, ¿A que instancia debes dirigirte?",
				answer: "sat",
				position: 2,
				orientation: "down",
				startx: 9,
				starty: 10
				},
				{
				clue: "El domicilio registrado en tu estado de cuenta es incorrecto, ¿A que instancia debes dirigirte?",
				answer: "afore",
				position: 4,
				orientation: "down",
				startx: 8,
				starty: 2
				},
				{
				clue: "El RFC registrado en tu cuenta es incorrecto, ¿A que instancia debes dirigirte?",
				answer: "sat",
				position: 6,
				orientation: "down",
				startx: 4,
				starty: 7
				}
			]

		$('#puzzle-wrapper').crossword(puzzleData);

	})

})(jQuery)
