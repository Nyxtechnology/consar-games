
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
            build: {
                files: {
                'static/build/dressup.min.js': ['static/build/dressup.js']
            }
        }
    },
    concat: {
          build: {
            src: ['src/modulo1/src/**/*.js', '!src/modulo1/src/*.js', 'src/modulo1/src/game.js'],
            dest: 'static/build/dressup.js'
          }
    },
    connect: {
      server: {
        options: {
          port: 9000,
          base: './'
        }
      }
    }
 })

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask("default", [
    "concat:build",
    "uglify:build"
    ]);

  grunt.registerTask('serve', [
    'connect:server:keepalive'
    ]);




};
