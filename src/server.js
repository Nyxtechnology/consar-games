import path from 'path'
import express from 'express'
import compression from 'compression'
import helmet from 'helmet'
import morgan from 'morgan'
import chalk from 'chalk'

const PORT = 3007
const log = console.log
const NODE_ENV = process.env.NODE_ENV || 'development'
const app = express()

app
  .use(helmet({
    frameguard: {
      action: 'allow-from',
      domain: 'http://cursoconsar.azurewebsites.net/'
    }
  }))
  .use(compression())
  .use(morgan('dev'))
  .use('/static', express.static(path.join(__dirname, '../static')))
  .get('/modulo1', (req, res) => res.sendFile(path.join(__dirname, './modulo1/index.html')))
  .get('/modulo1g', (req, res) => res.sendFile(path.join(__dirname, './modulo1g/index.html')))
  .get('/modulo2', (req, res) => res.sendFile(path.join(__dirname, './modulo2/index.html')))
  .get('/modulo3', (req, res) => res.sendFile(path.join(__dirname, './modulo3/index.html')))
  .get('/modulo4', (req, res) => res.sendFile(path.join(__dirname, './modulo4/index.html')))
  .get('/modulo5', (req, res) => res.sendFile(path.join(__dirname, './modulo5/index.html')))
  .get('/modulo6', (req, res) => res.sendFile(path.join(__dirname, './modulo6/index.html')))
  .get('/modulo7', (req, res) => res.sendFile(path.join(__dirname, './modulo7/index.html')))
  .get('/modulo8', (req, res) => res.sendFile(path.join(__dirname, './modulo8/index.html')))
  .get('/modulo9', (req, res) => res.sendFile(path.join(__dirname, './modulo9/index.html')))
  .listen(PORT, err => log(chalk.green(err || `Listening at port ${PORT} in ${NODE_ENV}`)))

  /*
  process.on('uncaughtException', err =>
    err.code === 'EADDRINUSE' ? log(chalk.red(`Port ${PORT} in use`)) : log(chalk.red(err.code)))
  */
